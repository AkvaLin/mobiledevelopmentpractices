//
//  MainView.swift
//  Pr4
//
//  Created by Никита Пивоваров on 12.03.2024.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            ContentView()
                .tabItem { Label(
                    title: { Text("AvgTime") },
                    icon: { Image(systemName: "1.circle") }
                ) }
            ThreadsView()
                .tabItem { Label(
                    title: { Text("Threads") },
                    icon: { Image(systemName: "2.circle") }
                ) }
            QueueView()
                .tabItem { Label(
                    title: { Text("Queue") },
                    icon: { Image(systemName: "3.circle") }
                ) }
            PublisherView()
                .tabItem { Label(
                    title: { Text("Publisher") },
                    icon: { Image(systemName: "4.circle") }
                ) }
            BackgroundView()
                .tabItem { Label(
                    title: { Text("Background") },
                    icon: { Image(systemName: "5.circle") }
                ) }
        }
    }
}
