//
//  Pr4App.swift
//  Pr4
//
//  Created by Никита Пивоваров on 12.03.2024.
//

import SwiftUI
import BackgroundTasks

@main
struct Pr4App: App {
    
    private let id = "vdc.Pr3"
    
    var body: some Scene {
        WindowGroup {
            MainView()
                .onAppear {
                    doSmthInBackground()
                    registerCounterBackgroundTask()
                }
        }
    }
    
    // MARK: - Setup background task
    
    private func doSmthInBackground() {
        
        BGTaskScheduler.shared.register(
            forTaskWithIdentifier: id,
            using: .global()
        ) { task in
            guard let task = task as? BGProcessingTask else { return }
            self.handleTask(task: task)
        }
    }
    
    private func registerCounterBackgroundTask() {
        BGTaskScheduler.shared.register(
            forTaskWithIdentifier: id + ".counter",
            using: .global()
        ) { task in
            guard let task = task as? BGAppRefreshTask else { return }
            task.expirationHandler = {
                task.setTaskCompleted(success: false)
            }
            
            let count = UserDefaults.standard.integer(forKey: "task_run_count")
            UserDefaults.standard.set(count + 1, forKey: "task_run_count")
            print("Counter increased")
            task.setTaskCompleted(success: true)
        }
    }
    
    private func handleTask(task: BGProcessingTask) {
        task.expirationHandler = {
            task.setTaskCompleted(success: false)
        }
        
        Task {
            NSLog("%@", "Start")
            try await Task.sleep(nanoseconds: UInt64(10  * Double(NSEC_PER_SEC)))
            NSLog("%@", "End")
            task.setTaskCompleted(success: true)
        }
    }
}
