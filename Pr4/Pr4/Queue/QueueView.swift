//
//  QueueView.swift
//  Pr4
//
//  Created by Никита Пивоваров on 13.03.2024.
//

import SwiftUI

struct QueueView: View {
    
    @State private var age = ""
    @State private var job = ""
    
    var body: some View {
        VStack {
            TextField("Age", text: $age)
            TextField("Job", text: $job)
            Button("Submit") {
                guard let age = Int(age) else { return }
                Task {
                    try await Task.sleep(nanoseconds: UInt64(Double(age)  * Double(NSEC_PER_SEC)))
                    DispatchQueue.global(qos: .userInitiated).async {
                        NSLog("%@", "Возраст: \(age). Должность: \(job)")
                    }
                }
            }
        }
        .padding()
        .textFieldStyle(.roundedBorder)
    }
}

#Preview {
    QueueView()
}
