//
//  PublisherViewModel.swift
//  Pr4
//
//  Created by Никита Пивоваров on 13.03.2024.
//

import Foundation
import Combine
import CryptoKit

class PublisherViewModel: ObservableObject {
    
    @Published var phrase = ""
    @Published var showAlert = false
    @Published var alertMessage = ""
    
    private var key = SymmetricKey(size: SymmetricKeySize(bitCount: 128))
    private var storage = Set<AnyCancellable>()
    @Published var encryptedPhrase: AES.GCM.SealedBox?
    
    public func setupPubisher() {
        $encryptedPhrase
            .subscribe(on: DispatchQueue.global(qos: .userInitiated))
            .map { [weak self] box in
                guard
                    let self = self,
                    let box = box
                else { return Optional<String>.none }
                return self.decrypt(sealedbox: box, key: self.key)
            }
            .receive(on: RunLoop.main)
            .sink { [weak self] message in
                guard
                    let message = message,
                    let self = self
                else { return }
                self.alertMessage = message
            }
            .store(in: &storage)
    }
    
    public func buttonTapped() {
        let encrypted = encrypt(message: phrase)
        encryptedPhrase = encrypted
    }
    
    private func encrypt(message: String) -> AES.GCM.SealedBox? {
        guard let data = message.data(using: .utf8) else { return nil }
        let encryptedData = try? AES.GCM.seal(data, using: key)
        return encryptedData
    }
    
    private func decrypt(sealedbox: AES.GCM.SealedBox, key: SymmetricKey) -> String? {
        do {
            let data = try AES.GCM.open(sealedbox, using: key)
            if let message = String(data: data, encoding: .utf8) {
                return message
            }
            return nil
        } catch {
            return nil
        }
    }
}
