//
//  PublisherView.swift
//  Pr4
//
//  Created by Никита Пивоваров on 13.03.2024.
//

import SwiftUI

struct PublisherView: View {
    
    @StateObject private var viewModel = PublisherViewModel()
    
    var body: some View {
        VStack {
            TextField("Phrase", text: $viewModel.phrase)
                .textFieldStyle(.roundedBorder)
            Button("Encrypt") {
                viewModel.buttonTapped()
            }
        }
        .padding()
        .onAppear {
            viewModel.setupPubisher()
        }
        .alert(viewModel.alertMessage, isPresented: $viewModel.showAlert) {
            Button("Ok", role: .cancel) { }
        }
        .onChange(of: viewModel.alertMessage) {
            viewModel.showAlert.toggle()
        }
    }
}

#Preview {
    PublisherView()
}
