//
//  ContentViewModel.swift
//  Pr4
//
//  Created by Никита Пивоваров on 12.03.2024.
//

import Foundation

class ContentViewModel: ObservableObject {
    
    @Published var text = "Здесь появится среднее кол-во пар в день"
    @Published var days = ""
    @Published var lessons = ""
    @Published var isTextFieldsDisabled = false
    @Published var isButtonDisabled = true
    @Published var threadName = Thread.current.name
    public let oldThreadName = Thread.current.name
    
    init() {
        Thread.current.name = "МОЙ НОМЕР ГРУППЫ: БСБО-10-21, НОМЕР ПО СПИСКУ: 20, МОЙ ЛЮБИИМЫЙ ФИЛЬМ: ???"
        threadName = Thread.current.name
    }
    
    public func checkIsButtonDisabled() {
        if let days = Int(days),
           let _ = Int(lessons),
           days > 0 {
            isButtonDisabled = false
        } else {
            isButtonDisabled = true
        }
    }
    
    public func calculateAvgLessonsAmountPerDay() {
        guard
            let days = Double(days),
            let lessons = Double(lessons),
            days > 0
        else { return }
        
        self.isTextFieldsDisabled = true
        self.days = ""
        self.lessons = ""
        self.isButtonDisabled = true
        
        Thread {
            let amount = lessons / days
            
            DispatchQueue.main.async {
                self.text = "Среднее кол-во пар: " + String(format: "%.1f", amount)
                self.isTextFieldsDisabled = false
            }
        }.start()
    }
}
