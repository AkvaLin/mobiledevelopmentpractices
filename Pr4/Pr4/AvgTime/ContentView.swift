//
//  ContentView.swift
//  Pr4
//
//  Created by Никита Пивоваров on 12.03.2024.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject private var viewModel = ContentViewModel()
    
    var body: some View {
        VStack {
            Spacer()
            Group {
                TextField("Кол-во пар", text: $viewModel.lessons)
                    .disabled(viewModel.isTextFieldsDisabled)
                TextField("Кол-во дней", text: $viewModel.days)
                    .disabled(viewModel.isTextFieldsDisabled)
                Text(viewModel.text)
            }
            .multilineTextAlignment(.center)
            .textFieldStyle(.roundedBorder)
            .padding([.bottom, .horizontal])
            Button("Perform") {
                viewModel.calculateAvgLessonsAmountPerDay()
            }
            .disabled(viewModel.isButtonDisabled)
            Spacer()
            Group {
                Text("Current Thread: \(viewModel.threadName ?? "Empty")")
                Text("Old name: \(viewModel.oldThreadName ?? "Empty")")
                    .padding(.top)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
        }
        .padding()
        .onChange(of: viewModel.lessons) {
            viewModel.checkIsButtonDisabled()
        }
        .onChange(of: viewModel.days) {
            viewModel.checkIsButtonDisabled()
        }
    }
}

#Preview {
    ContentView()
}
