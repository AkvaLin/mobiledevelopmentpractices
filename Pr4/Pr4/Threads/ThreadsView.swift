//
//  ThreadsView.swift
//  Pr4
//
//  Created by Никита Пивоваров on 12.03.2024.
//

import SwiftUI

struct ThreadsView: View {
    
    @State private var text = ""
    
    private let description = """
        Вывод будет иметь следующую последовательность: text1, text2, text3, т.к. сначала в главный поток будет добавлена первая задача (спустя 2 секуеды ожидания), затем будет запущен таймер на добавление третьей задачи (спустя секунду ожидания после добавления первой задачи), и сразу после этого будет добавлена вторая задача. Способ добавления задачи, будь то MainActor или DispatchQueue, не будет влиять на порядок их выполнения.
    """
    
    var body: some View {
        VStack {
            Text(description)
                .multilineTextAlignment(.center)
                .padding()
                .background {
                    RoundedRectangle(cornerRadius: 12)
                        .stroke()
                }
            Text(text)
                .padding(.top)
        }
        .padding()
        .onAppear(perform: {
            let task1 = DispatchWorkItem(block: {
                text += "text1 "
            })
            let task2 = DispatchWorkItem(block: {
                text += "text2 "
            })
            let task3 = DispatchWorkItem(block: {
                text += "text3 "
            })
            Task {
                try await Task.sleep(nanoseconds: UInt64(2  * Double(NSEC_PER_SEC)))
                performTask(item: task1)
                try await Task.sleep(nanoseconds: UInt64(1  * Double(NSEC_PER_SEC)))
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    task3.perform()
                })
                DispatchQueue.main.async {
                    task2.perform()
                }
            }
        })
    }
    
    @MainActor
    private func performTask(item: DispatchWorkItem) {
        item.perform()
    }
}

#Preview {
    ThreadsView()
}
