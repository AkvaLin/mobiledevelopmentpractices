//
//  BackgroundView.swift
//  Pr4
//
//  Created by Никита Пивоваров on 13.03.2024.
//

import SwiftUI
import BackgroundTasks

struct BackgroundView: View {
    
    private let id = "vdc.Pr3"
    
    var body: some View {
        VStack {
            Text("Hello, Background!")
                .onAppear {
                    schedule()
                    scheduleTaskCounter()
                }
            Text("Count: \(UserDefaults.standard.integer(forKey: "task_run_count"))")
        }
    }
    
    private func schedule() {
        // Manual test: e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"vdc.Pr3"]
        do {
            let newTask = BGProcessingTaskRequest(identifier: id)
            newTask.requiresNetworkConnectivity = true
            try BGTaskScheduler.shared.submit(newTask)
            NSLog("%@", "Submitted")
        } catch {
            
        }
    }
    
    private func scheduleTaskCounter() {
        // Manual test: e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"vdc.Pr3.counter"]
        do {
            let newTask = BGAppRefreshTaskRequest(identifier: id + ".counter")
            try BGTaskScheduler.shared.submit(newTask)
            NSLog("%@", "Submitted counter")
        } catch let error {
            print(error)
        }
    }
}
