//
//  MapViewModel.swift
//  Pr8
//
//  Created by Никита Пивоваров on 01.04.2024.
//

import Foundation
import MapKit

class MapViewModel: ObservableObject {
    
    @Published var searchText = ""
    @Published var searchResults = [MKMapItem]()
    @Published var selectedResult: MKMapItem?
    @Published var visibleRegion: MKCoordinateRegion?
    
    public func search() {
        
        guard let visibleRegion = visibleRegion else { return }
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchText
        request.resultTypes = .pointOfInterest
        request.region = MKCoordinateRegion(
            center: visibleRegion.center,
            span: visibleRegion.span
        )
        
        Task {
            let search = MKLocalSearch(request: request)
            let response = try? await search.start()
            DispatchQueue.main.async {
                self.searchResults = response?.mapItems ?? []
            }
        }
    }
}
