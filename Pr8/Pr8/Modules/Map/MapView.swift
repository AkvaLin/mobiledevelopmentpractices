//
//  MapView.swift
//  Pr8
//
//  Created by Никита Пивоваров on 29.03.2024.
//

import SwiftUI
import MapKit

struct MapView: View {
    
    @StateObject private var viewModel = MapViewModel()
    @FocusState private var textFieldIsFocused: Bool
    
    var body: some View {
        ZStack {
            Map(selection: $viewModel.selectedResult) {
                UserAnnotation()
                ForEach(viewModel.searchResults, id: \.self) { result in
                    Marker(item: result)
                }
            }
            .onMapCameraChange { context in
                viewModel.visibleRegion = context.region
            }
            .onTapGesture {
                textFieldIsFocused = false
            }
            .safeAreaInset(edge: .bottom) {
                VStack {
                    if let result = viewModel.selectedResult {
                        VStack(spacing: 8) {
                            if
                                let country = result.placemark.country,
                                let city = result.placemark.locality,
                                let address = result.placemark.thoroughfare,
                                let subAddress = result.placemark.subThoroughfare {
                                Text([country, city, address, subAddress].joined(separator: ", "))
                                    .font(.caption2)
                            }
                            if let name = result.name {
                                Text(name)
                                    .font(.title2)
                            }
                            if let number = result.phoneNumber {
                                Text(number)
                                    .font(.caption)
                            }
                            if let url = result.url {
                                Text(url.absoluteString)
                                    .font(.caption)
                            }
                        }
                        .padding()
                        .background {
                            RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                                .fill(.regularMaterial)
                        }
                        .padding([.horizontal, .bottom])
                    }
                    
                    HStack {
                        TextField("Point of interest", text: $viewModel.searchText)
                            .focused($textFieldIsFocused)
                        Button {
                            viewModel.search()
                        } label: {
                            Image(systemName: "magnifyingglass")
                        }
                        .foregroundStyle(.secondary)
                    }
                    .padding()
                    .background {
                        RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                            .fill(.regularMaterial)
                    }
                    .padding([.horizontal, .bottom])
                }
            }
        }
    }
}

#Preview {
    MapView()
}
