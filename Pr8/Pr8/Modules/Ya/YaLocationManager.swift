//
//  LocationManager.swift
//  Pr8
//
//  Created by Никита Пивоваров on 28.03.2024.
//

import Foundation
import CoreLocation
import YandexMapsMobile
import Combine

class YaLocationManager: NSObject, ObservableObject {
    
    @Published var lastUserLocation: CLLocation? = nil
    @Published var showDetails = false
    
    lazy var map: YMKMap? = {
        return mapView.mapWindow.map
    }()
    
    private let drivingRouter = YMKDirections.sharedInstance().createDrivingRouter(withType: .combined)
    private let manager = CLLocationManager()
    private var isFocused = true
    private var drivingSession: YMKDrivingSession?
    
    let mapView: YMKMapView = YMKMapView(frame: CGRect.zero)
    
    override init() {
        super.init()
        manager.delegate = self
        manager.startUpdatingLocation()
    }
    
    private func centerMapLocation(target location: YMKPoint?, map: YMKMapView) {
        guard let location = location else { print("Failed to get user location"); return }
        map.mapWindow.map.move(
            with: YMKCameraPosition(target: location, zoom: 18, azimuth: 0, tilt: 0),
            animation: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.5)
        )
    }
    
    func submitRequest() {
        guard let lastUserLocation = lastUserLocation else { return }
        
        let drivingOptions: YMKDrivingOptions = {
            let options = YMKDrivingOptions()
            options.routesCount = 2
            return options
        }()
        let vehicleOptions = YMKDrivingVehicleOptions()
        
        let points = [
            YMKRequestPoint(point: YMKPoint(latitude: lastUserLocation.coordinate.latitude, 
                                            longitude: lastUserLocation.coordinate.longitude),
                            type: .waypoint,
                            pointContext: nil,
                            drivingArrivalPointId: nil),
            YMKRequestPoint(point: YMKPoint(latitude: 55.719710,
                                            longitude: 37.685055),
                            type: .waypoint,
                            pointContext: nil,
                            drivingArrivalPointId: nil)
        ]
        
        drivingSession = drivingRouter.requestRoutes(
            with: points,
            drivingOptions: drivingOptions,
            vehicleOptions: vehicleOptions,
            routeHandler: drivingRouteHandler
        )
    }
    
    private func drivingRouteHandler(drivingRoutes: [YMKDrivingRoute]?, error: Error?) {
        if let error {
            print(error)
            return
        }
        
        guard let drivingRoutes else {
            return
        }
        let colors: [UIColor] = [.red, .green]
        
        for (index, route) in drivingRoutes.enumerated() {
            map?.mapObjects.addPolyline(with: route.geometry).setStrokeColorWith(colors[index])
        }
        
        let placemark = map?.mapObjects.addPlacemark()
        placemark?.geometry = YMKPoint(latitude: 55.719710,
                                       longitude: 37.685055)
        placemark?.addTapListener(with: self)
        placemark?.setTextWithText(
            "Miks Karting",
            style: {
                let textStyle = YMKTextStyle()
                textStyle.size = 10.0
                textStyle.placement = .right
                textStyle.offset = 5.0
                return textStyle
            }()
        )

        let compositeIcon = placemark?.useCompositeIcon()
        compositeIcon?.setIconWithName(
            "pin",
            image: UIImage(systemName: "car.fill")!,
            style: {
                let iconStyle = YMKIconStyle()
                iconStyle.anchor = NSValue(cgPoint: CGPoint(x: 0.5, y: 1.0))
                iconStyle.scale = 0.9
                return iconStyle
            }()
        )
    }
    
    func requestAuthorization() {
        if manager.authorizationStatus != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        }
    }
    
    func currentUserLocation() {
        if let myLocation = lastUserLocation {
            centerMapLocation(target: YMKPoint(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude), map: mapView)
        }
    }
}

extension YaLocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.lastUserLocation = locations.last
        if isFocused {
            currentUserLocation()
            isFocused.toggle()
            submitRequest()
        }
    }
}

extension YaLocationManager: YMKMapObjectTapListener {
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        showDetails = true
        return true
    }
}
