//
//  YandexMapView.swift
//  Pr8
//
//  Created by Никита Пивоваров on 28.03.2024.
//

import SwiftUI
import YandexMapsMobile
import Combine

struct YandexMapViewRepresentable: UIViewRepresentable {
    
    @EnvironmentObject var locationManager: YaLocationManager
    
    func makeUIView(context: Context) -> YMKMapView {
        return locationManager.mapView
    }
    
    func updateUIView(_ mapView: YMKMapView, context: Context) {}
}
