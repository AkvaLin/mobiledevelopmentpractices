//
//  ContentView.swift
//  Pr8
//
//  Created by Никита Пивоваров on 28.03.2024.
//

import SwiftUI
import YandexMapsMobile

struct YandexMapView: View {
    
    @ObservedObject var locationManager = YaLocationManager()
    
    var body: some View {
        ZStack{
            YandexMapViewRepresentable()
                .environmentObject(locationManager)
        }
        .onAppear {
            locationManager.requestAuthorization()
        }
        .popover(isPresented: $locationManager.showDetails) {
            DetailsView(image: Image(systemName: "car"),
                        title: "Miks karting",
                        description: "Miks Karting — это крытый картодром, расположенный недалеко от станции метро Волгоградский проспект. Это отличное место для тех, кто хочет получить новые впечатления и выплеснуть адреналин.")
        }
        .ignoresSafeArea()
    }
}

#Preview {
    YandexMapView()
}
