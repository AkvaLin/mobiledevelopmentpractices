//
//  OSMViewController.swift
//  Pr8
//
//  Created by Никита Пивоваров on 28.03.2024.
//

import UIKit
import SwiftUI
import MapKit

class OSMViewController: UIViewController {
    
    let mapView = {
        let view = MKMapView()
        let kislovodskAnnotation = MKPointAnnotation()
        kislovodskAnnotation.title = "Kislovodsk"
        kislovodskAnnotation.coordinate = CLLocationCoordinate2D(latitude: 43.910558, longitude: 42.717787)
        view.addAnnotation(kislovodskAnnotation)
        let pyatigorskAnnotation = MKPointAnnotation()
        pyatigorskAnnotation.title = "Pyatigorsk"
        pyatigorskAnnotation.coordinate = CLLocationCoordinate2D(latitude: 44.031781, longitude: 43.045491)
        view.addAnnotation(pyatigorskAnnotation)
        return view
    }()
    
    let stackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillEqually
        view.spacing = 10
        return view
    }()
    
    lazy var plusButton = {
        let bttn = UIButton()
        bttn.setImage(UIImage(systemName: "plus"), for: .normal)
        bttn.tintColor = .secondaryLabel
        bttn.layer.borderWidth = 1
        bttn.layer.cornerRadius = 12
        bttn.layer.borderColor = UIColor.secondaryLabel.cgColor
        bttn.addTarget(self, action: #selector(zoomIn), for: .touchUpInside)
        return bttn
    }()
    
    lazy var minusButton = {
        let bttn = UIButton()
        bttn.setImage(UIImage(systemName: "minus"), for: .normal)
        bttn.tintColor = .secondaryLabel
        bttn.layer.borderWidth = 1
        bttn.layer.cornerRadius = 12
        bttn.layer.borderColor = UIColor.secondaryLabel.cgColor
        bttn.addTarget(self, action: #selector(zoomOut), for: .touchUpInside)
        return bttn
    }()
    
    var tileRenderer: MKTileOverlayRenderer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.cameraZoomRange = MKMapView.CameraZoomRange(
            minCenterCoordinateDistance: 700,
            maxCenterCoordinateDistance: .infinity)
        mapView.showsCompass = true
        mapView.showsUserLocation = true
        mapView.showsScale = true
        let initialRegion = MKCoordinateRegion(.world)
        mapView.cameraBoundary = MKMapView.CameraBoundary(
            coordinateRegion: initialRegion)
        view.addSubview(mapView)
        mapView.addSubview(stackView)
        mapView.frame = view.bounds
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: mapView.centerYAnchor),
            stackView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -10),
            stackView.widthAnchor.constraint(equalToConstant: 44),
            stackView.heightAnchor.constraint(equalToConstant: 98)
        ])
        stackView.addArrangedSubview(plusButton)
        stackView.addArrangedSubview(minusButton)
        
        setupTileRenderer()
    }
    
    private func setupTileRenderer() {
        let overlay = AdventureMapOverlay()
        overlay.canReplaceMapContent = true
        tileRenderer = MKTileOverlayRenderer(tileOverlay: overlay)
        mapView.addOverlay(overlay, level: .aboveLabels)
    }
    
    @objc private func zoomIn() {
        var region = mapView.region
        region.span.latitudeDelta /= 2.0
        region.span.longitudeDelta /= 2.0
        mapView.setRegion(region, animated: true)
    }
    
    @objc private func zoomOut() {
        var region = mapView.region
        region.span.latitudeDelta = min(region.span.latitudeDelta  * 2.0, 200)
        region.span.longitudeDelta = min(region.span.longitudeDelta  * 2.0, 200)
        mapView.setRegion(region, animated: true)
    }
}

extension OSMViewController: MKMapViewDelegate {
    func mapView(
        _ mapView: MKMapView,
        rendererFor overlay: MKOverlay
    ) -> MKOverlayRenderer {
        guard let tileRenderer = tileRenderer else { fatalError() }
        return tileRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? MKPointAnnotation else { return }
        guard let annotationTitle = annotation.title else { return }
        let detailsView = DetailsView(image: Image(systemName: "mountain.2.fill"), title: annotationTitle, description: "Home")
        let vc = UIHostingController(rootView: detailsView)
        vc.modalPresentationStyle = .popover
        present(vc, animated: true)
    }
}

class AdventureMapOverlay: MKTileOverlay {
    override func url(forTilePath path: MKTileOverlayPath) -> URL {
        let tileUrl =
        "https://tile.openstreetmap.org/\(path.z)/\(path.x)/\(path.y).png"
        return URL(string: tileUrl)!
    }
}

