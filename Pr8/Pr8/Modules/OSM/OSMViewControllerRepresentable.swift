//
//  OSMViewControllerRepresentable.swift
//  Pr8
//
//  Created by Никита Пивоваров on 28.03.2024.
//

import SwiftUI

struct OSMViewControllerRepresentable: UIViewControllerRepresentable {
    
    typealias UIViewControllerType = OSMViewController
    
    func makeUIViewController(context: Context) -> OSMViewController {
        let vc = OSMViewController()
        return vc
    }
    
    func updateUIViewController(_ uiViewController: OSMViewController, context: Context) {}
    
}
