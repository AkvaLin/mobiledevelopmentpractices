//
//  OSMView.swift
//  Pr8
//
//  Created by Никита Пивоваров on 01.04.2024.
//

import SwiftUI

struct OSMView: View {
    var body: some View {
        VStack {
            OSMViewControllerRepresentable()
                .ignoresSafeArea()
        }
    }
}

#Preview {
    OSMView()
}
