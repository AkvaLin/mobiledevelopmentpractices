//
//  Pr8App.swift
//  Pr8
//
//  Created by Никита Пивоваров on 28.03.2024.
//

import SwiftUI
import YandexMapsMobile

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        YMKMapKit.setApiKey("dabbdd73-0166-42ed-8fdd-a3e71815acbf")
        YMKMapKit.sharedInstance()
        return true
    }
}

@main
struct Pr8App: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    @State private var showYandex = false
    @State private var showOSM = false
    @State private var showApple = false
    
    var body: some Scene {
        WindowGroup {
            NavigationStack {
                VStack(spacing: 20) {
                    NavigationLink("Yandex") {
                        YandexMapView()
                    }
                    NavigationLink("OSM") {
                        OSMView()
                    }
                    NavigationLink("Apple") {
                        MapView()
                    }
                }
            }
        }
    }
}
