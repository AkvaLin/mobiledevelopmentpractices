//
//  DetailsView.swift
//  Pr8
//
//  Created by Никита Пивоваров on 29.03.2024.
//

import SwiftUI

struct DetailsView: View {
    
    let image: Image
    let title: String
    let description: String
    
    var body: some View {
        VStack {
            HStack(alignment: .bottom) {
                image
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 100, maxHeight: 100)
                    .padding(.trailing)
                Text(title)
                    .font(.largeTitle)
            }
            Spacer()
            Text(description)
            Spacer()
        }
        .padding()
    }
}

#Preview {
    DetailsView(image: Image(systemName: "car"), title: "Miks karting", description: "masdklfmnaskdjfnka")
}
