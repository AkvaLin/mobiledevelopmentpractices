//
//  BookViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 09.03.2024.
//

import UIKit

class BookViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    var book: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if book.isEmpty {
            label.text = "Тут появится название вашей любимой книги!"
        } else {
            label.text = "Название Вашей любимой книги: \(book)"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "bookTransfer" else { return }
        guard let destination = segue.destination as? BookInputViewController else { return }
        destination.clouser = { [weak self] book in
            guard let self = self else { return }
            self.book = book
        }
        if !book.isEmpty {
            destination.book = book
        }
    }
}
