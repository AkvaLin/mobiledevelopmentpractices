//
//  LinksViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 11.03.2024.
//

import UIKit
import MapKit

class LinksViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func callButtonPressed(_ sender: Any) {

        let phoneNumber = "89811112233"

        if let phoneURL = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(phoneURL) {
            UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func browserButtonPressed(_ sender: Any) {
        if let url = URL(string: "http://developer.android.com"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func mapButtonPressed(_ sender: Any) {
        let latitude = 55.749479
        let longitude = 37.613944
        
        let geoURLString = "maps://?q=\(latitude),\(longitude)"

        if let geoURL = URL(string: geoURLString), UIApplication.shared.canOpenURL(geoURL) {
            UIApplication.shared.open(geoURL, options: [:], completionHandler: nil)
        }
    }
}
