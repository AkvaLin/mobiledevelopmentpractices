//
//  BookInputViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 09.03.2024.
//

import UIKit
import Combine

class BookInputViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var book: String?
    var clouser: ((String) -> Void)?
    var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        textField.delegate = self
        
        let textFieldPublisher = NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: textField)
            .map( {
                ($0.object as? UITextField)?.text
            })
            .debounce(for: 0.5, scheduler: DispatchQueue.global(qos: .userInteractive))
            .removeDuplicates()
        
        textFieldPublisher
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] value in
                guard let self = self else { return }
                self.label.text = "Любимая книга разработчика: \(self.textField.text ?? "")"
            })
            .store(in: &cancellables)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let book = book {
            textField.text = book
        }
        label.text = "Любимая книга разработчика: \(textField.text ?? "")"
    }
    
    
    @IBAction func send(_ sender: Any) {
        guard
            let text = textField.text,
            let clouser = clouser,
            !text.isEmpty
        else { return }
        clouser(text)
    }
}

extension BookInputViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
