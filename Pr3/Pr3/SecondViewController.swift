//
//  SecondViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 09.03.2024.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    var time: (Int, Int)?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let time = time {
            var timeString = "\(time.1)"
            if time.1 < 10 {
                timeString = "0" + timeString
            }
            let text = "КВАДРАТ ЗНАЧЕНИЯ МОЕГО НОМЕРА ПО СПИСКУ В ГРУППЕ СОСТАВЛЯЕТ  \(pow(20, 2)), а текущее время \(time.0):\(timeString)"
            label.text = text
        }
    }
}
