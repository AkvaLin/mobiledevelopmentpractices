//
//  ContainerViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 11.03.2024.
//

import UIKit

class ContainerViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private lazy var firstViewController: UIViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Container1")
        self.add(asChildViewController: viewController)

        return viewController
    }()

    private lazy var secondViewController: UIViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Container2")
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        
        if UIDevice.current.orientation.isLandscape {
            segmentedControl.isHidden = true
        } else {
            segmentedControl.isHidden = false
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: any UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if UIDevice.current.orientation.isLandscape {
            segmentedControl.isHidden = true
            remove(asChildViewController: firstViewController)
            remove(asChildViewController: secondViewController)
        } else {
            remove(asChildViewController: firstViewController)
            remove(asChildViewController: secondViewController)
            segmentedControl.isHidden = false
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if UIDevice.current.orientation.isLandscape {
            addLandscape()
        } else {
            updateView()
        }
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        
        view.addSubview(viewController.view)

        viewController.view.frame = view.bounds

        viewController.didMove(toParent: self)
    }
    
    private func addLandscape() {
        addChild(firstViewController)
        addChild(secondViewController)
        
        view.addSubview(firstViewController.view)
        view.addSubview(secondViewController.view)
        
        firstViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width / 3, height: view.frame.height)
        secondViewController.view.frame = CGRect(x: firstViewController.view.frame.maxX, y: 0, width: view.frame.width / 3 * 2, height: view.frame.height)
        
        firstViewController.didMove(toParent: self)
        secondViewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: secondViewController)
            add(asChildViewController: firstViewController)
        } else {
            remove(asChildViewController: firstViewController)
            add(asChildViewController: secondViewController)
        }
    }
    
    @objc private func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
}
