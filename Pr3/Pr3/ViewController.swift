//
//  ViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 09.03.2024.
//

import UIKit

class ViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "timeTransfer" else { return }
        guard let destination = segue.destination as? SecondViewController else { return }
        let date = Date()
        let calendar = Calendar.current
        let hours = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        destination.time = (hours, minutes)
    }
}
