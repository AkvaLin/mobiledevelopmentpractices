//
//  FxProViewController.swift
//  Pr3
//
//  Created by Никита Пивоваров on 11.03.2024.
//

import UIKit

class FxProViewController: UIViewController {
    
    private var fxProView: FxProView?
    private var blurView: UIVisualEffectView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fxProView = FxProView()
        fxProView?.injectIn(controller: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let tabBarController = tabBarController {
            let blurEffect = UIBlurEffect(style: .dark)
            blurView = UIVisualEffectView(effect: blurEffect)
            if let blurView = blurView {
                blurView.frame = tabBarController.tabBar.bounds
                blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                tabBarController.tabBar.insertSubview(blurView, at: 0)
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let blurView = blurView {
            blurView.removeFromSuperview()
        }
    }
}
