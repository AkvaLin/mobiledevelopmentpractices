//
//  FxProView.swift
//  Pr3
//
//  Created by Никита Пивоваров on 11.03.2024.
//

import SwiftUI

struct FxProView: View {
    
    @State private var isWebViewPresented = false
    
    var body: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            ScrollView {
                VStack {
                    VStack {
                        Text("FX Pro Formula Steering Wheel")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .padding()
                        Text("Co-developed with formula racers and engineers Formula Xtreme Pro, for the ultimate formula racing experience")
                            .multilineTextAlignment(.center)
                            .padding(.horizontal, 60)
                        Image(.wheel)
                            .resizable()
                            .scaledToFit()
                            .padding()
                            .background {
                                RadialGradient(colors: [.gradientBlue, .black],
                                               center: .center,
                                               startRadius: 5,
                                               endRadius: 200)
                            }
                    }
                    .foregroundStyle(.white)
                    VStack(alignment: .leading) {
                        Group {
                            Label(
                                title: { Text("Next Gen Driver: SimPro Manager") },
                                icon: {
                                    Image(.driver)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("290mm") },
                                icon: {
                                    Image(.wheelIcon)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("5mm High-quality Carbon Fiber") },
                                icon: {
                                    Image(.material)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("Custom Silicone Grip") },
                                icon: {
                                    Image(.grip)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("Patented New Botton Design") },
                                icon: {
                                    Image(.buttonDesign)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("SIMAGIC S-ray 12 RGB Buttons") },
                                icon: {
                                    Image(.sray)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("Patented HALL Paddle Modules") },
                                icon: {
                                    Image(.paddles)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("Patented Soft Glow LED lights") },
                                icon: {
                                    Image(.led)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                            Label(
                                title: { Text("Patented Button Stickers") },
                                icon: {
                                    Image(.stickers)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(maxWidth: 40)
                                }
                            )
                        }
                        .padding(.bottom, 4)
                        Label(
                            title: { Text("50mm SIMAGIC Quick Release") },
                            icon: {
                                Image(.qr)
                                    .resizable()
                                    .scaledToFit()
                                    .frame(maxWidth: 40)
                            }
                        )
                    }
                    .foregroundStyle(.white)
                    .frame(maxWidth: .infinity)
                    .padding(20)
                    .background {
                        RoundedRectangle(cornerRadius: 12)
                            .stroke(.white)
                    }
                    .padding()
                    Button {
                        isWebViewPresented.toggle()
                    } label: {
                        Text("Details")
                            .foregroundStyle(.white)
                            .padding(.vertical, 8)
                            .padding(.horizontal)
                            .background {
                                RoundedRectangle(cornerRadius: 12)
                                    .fill(.gradientBlue)
                            }
                            .frame(minWidth: 44, minHeight: 44)
                    }
                    .padding([.bottom, .horizontal])
                }
            }
            .scrollBounceBehavior(.basedOnSize)
        }
        .sheet(isPresented: $isWebViewPresented) {
            WebView(url: URL(string: "https://en.simagic.com/product/55")!)
        }
    }
}

#Preview {
    FxProView()
}
