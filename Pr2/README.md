# Pr2



| Navigation Before | Navigation After |
| ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/tc6qnFS/Screenshot-2024-03-07-at-23-05-12.png) | ![IMAGE_DESCRIPTION](https://i.ibb.co/3s3jmT5/Screenshot-2024-03-07-at-23-05-16.png) |

| ViewControllers Life Cycle |
| ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/z8CdNXx/Screenshot-2024-03-07-at-23-30-11.png) |

| Web | Browser | InApp |
| ------ | ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/MMyDR94/Screenshot-2024-03-07-at-23-16-23.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/sWs71RW/Screenshot-2024-03-07-at-23-16-26.png) | ![IMAGE_DESCRIPTION](https://i.ibb.co/Jsgddm9/Screenshot-2024-03-07-at-23-16-34.png) |

| Pickers | DatePicker | TimePicker | ColorPicker |
| ------ | ------ | ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/rH8whgq/Screenshot-2024-03-07-at-23-05-28.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/10hHNQR/Screenshot-2024-03-07-at-23-05-30.png) | ![IMAGE_DESCRIPTION](https://i.ibb.co/LCGxZxj/Screenshot-2024-03-07-at-23-05-33.png) | ![IMAGE_DESCRIPTION](https://i.ibb.co/1YcdgS2/Screenshot-2024-03-07-at-23-05-36.png) |
