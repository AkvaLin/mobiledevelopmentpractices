//
//  ThirdViewController.swift
//  Pr2
//
//  Created by Никита Пивоваров on 07.03.2024.
//

import UIKit
import SafariServices

class ThirdViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        guard let url = URL(string: "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func modularButtonTapped(_ sender: Any) {
        guard let url = URL(string: "https://google.com") else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
}
