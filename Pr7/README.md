# Pr7

## WebSocket (DateTime)

| | |
| ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/g6LLWWL/Simulator-Screenshot-i-Phone-15-Pro-2024-03-27-at-15-19-46.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/L1yzyHS/Simulator-Screenshot-i-Phone-15-Pro-2024-03-27-at-15-19-52.png) |

## HTTP (Weather)

| Main (light) | Main (dark) |
| :-------------: |:-------------:| 
| ![IMG_7099](https://i.ibb.co/k8NncBP/IMG-7099.png) | ![IMG_7102](https://i.ibb.co/WpZyspc/IMG-7102.png) |

| Search (light) | Search (dark) |
| :-------------: |:-------------:| 
| ![IMG_7100](https://i.ibb.co/vXNF5cz/IMG-7100.png) | ![IMG_7103](https://i.ibb.co/xsb9mbx/IMG-7103.png) |

## Firebase

| | |
| ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/xF9BVMW/Simulator-Screenshot-i-Phone-15-Pro-2024-03-27-at-15-19-58.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/1736vh2/Simulator-Screenshot-i-Phone-15-Pro-2024-03-27-at-15-20-22.png) |

## Sign In

| | |
| ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/wrkxj9t/Simulator-Screenshot-i-Phone-15-Pro-2024-03-27-at-15-19-25.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/25hgrGM/Simulator-Screenshot-i-Phone-15-Pro-2024-03-27-at-15-19-41.png) |
