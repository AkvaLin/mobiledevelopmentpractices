//
//  DateTimeView.swift
//  Pr7
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

struct DateTimeView: View {

    @StateObject var viewModel = DateTimeViewModel()
    
    var body: some View {
        VStack {
            Text(viewModel.timeText)
                .multilineTextAlignment(.center)
                .padding()
            Button("Получить время", action: viewModel.connect)
                .disabled(viewModel.isButtonDisabled)
                .padding()
                .background(Color.blue)
                .foregroundColor(.white)
                .cornerRadius(10)
        }
    }
}

#Preview {
    DateTimeView()
}
