//
//  DateTimeViewModel.swift
//  Pr7
//
//  Created by Никита Пивоваров on 19.03.2024.
//

import Foundation
import Starscream

class DateTimeViewModel: ObservableObject {
    
    @Published var timeText = "Нажмите кнопку, чтобы получить время"
    @Published var isButtonDisabled = false
    
    private var socket: WebSocket!
    private var urlString = "wss://time.cloudflare.com"
    
    func connect() {
        var request = URLRequest(url: URL(string: urlString)!)
        request.timeoutInterval = 30
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
}

struct DateData: Decodable {
    let date: String
}

extension DateTimeViewModel: WebSocketDelegate {
    func didReceive(event: Starscream.WebSocketEvent, client: any Starscream.WebSocketClient) {
        switch event {
        case .error(let error):
            guard let error = error as? HTTPUpgradeError else { break }
            let editedString = "\(error)".dropFirst(18).dropLast().replacingOccurrences(of: "[", with: "{").replacingOccurrences(of: "]", with: "}")
            guard let data = editedString.data(using: .utf8) else { return }
            guard let json = try? JSONSerialization.jsonObject(with: data) else { return }
            guard let dictionary = json as? [String: String] else { return }
            let dateArray = dictionary["Date", default: ""].replacingOccurrences(of: ",", with: "").components(separatedBy: " ")
            guard dateArray.count == 6 else { return }
            timeText = dateArray[1] + " " + dateArray[2] + " " + dateArray[3] + "\n" + dateArray[4]
        default:
            break
        }
    }
}


