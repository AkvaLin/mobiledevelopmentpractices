//
//  FirebaseView.swift
//  Pr7
//
//  Created by Никита Пивоваров on 26.03.2024.
//

import SwiftUI

struct FirebaseView: View {
    
    @StateObject private var viewModel = FirebaseViewModel()
    
    var body: some View {
        ZStack {
            Color.black.opacity(0.1)
                .ignoresSafeArea()
            VStack {
                Label(
                    title: { Text("Firebase") },
                    icon: {
                        Image(.firebaseIcon)
                            .resizable()
                            .scaledToFit()
                    }
                )
                .font(.largeTitle)
                .frame(height: 60)
                .padding(.horizontal, 40)
                .padding(.top, 80)
                Text("Email and Password")
                    .padding(.top, 80)
                if viewModel.isSignedIn {
                    Group {
                        Text(viewModel.emailText)
                        Text(viewModel.uid)
                    }
                    .font(.caption)
                    .padding(.horizontal)
                } else {
                    Text("Sign out")
                }
                Spacer()
                    .layoutPriority(1)
                ZStack {
                    Color.black.opacity(0.1)
                        .ignoresSafeArea()
                    HStack(alignment: .center, spacing: 20) {
                        VStack(spacing: 0) {
                            if viewModel.isSignedIn {
                                Button {
                                    viewModel.signOut()
                                } label: {
                                    Text("Sign Out")
                                        .frame(maxWidth: .infinity)
                                }
                                .buttonStyle(.borderedProminent)
                                .tint(.purple)
                                .padding(.top)
                            } else {
                                TextField("email", text: $viewModel.email)
                                    .textInputAutocapitalization(.never)
                                Divider()
                                    .overlay(.black)
                                Button {
                                    viewModel.signIn()
                                } label: {
                                    Text("Sign In")
                                        .frame(maxWidth: .infinity)
                                }
                                .buttonStyle(.borderedProminent)
                                .tint(.purple)
                                .padding(.top)
                            }
                        }
                        VStack(spacing: 0) {
                            if viewModel.isSignedIn {
                                Button {
                                    viewModel.requestVerification()
                                } label: {
                                    Text("Verify Email")
                                        .frame(maxWidth: .infinity)
                                }
                                .buttonStyle(.borderedProminent)
                                .tint(.purple)
                                .padding(.top)
                            } else {
                                SecureField("password", text: $viewModel.password)
                                    .textInputAutocapitalization(.never)
                                Divider()
                                    .overlay(.black)
                                Button {
                                    viewModel.register()
                                } label: {
                                    Text("Create Account")
                                        .frame(maxWidth: .infinity)
                                }
                                .buttonStyle(.borderedProminent)
                                .tint(.purple)
                                .padding(.top)
                            }
                        }
                    }
                    .padding(40)
                }
            }
        }
        .alert("Failed to register an account", 
               isPresented: $viewModel.showRegisterError,
               actions: {
            Button("Ok") {}
        },
               message: {
            Text(viewModel.registerErrorText)
        })
        .alert("Failed to login into account",
               isPresented: $viewModel.showLoginError,
               actions: {
            Button("Ok") {}
        },
               message: {
            Text(viewModel.loginErrorText)
        })
    }
}

#Preview {
    FirebaseView()
}
