//
//  FirebaseViewModel.swift
//  Pr7
//
//  Created by Никита Пивоваров on 26.03.2024.
//

import Foundation
import FirebaseCore
import FirebaseAuth

class FirebaseViewModel: ObservableObject {
    
    @Published var email = ""
    @Published var password = ""
    @Published var showRegisterError = false
    @Published var showLoginError = false
    @Published var isSignedIn = false
    
    private(set) var registerErrorText = ""
    private(set) var loginErrorText = ""
    private(set) var emailText = ""
    private(set) var uid = ""
    private var user: User?
    
    public func signIn() {
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
            guard let self = self else { return }
            guard let user = authResult?.user, error == nil else {
                if let error = error {
                    self.loginErrorText = error.localizedDescription
                    self.showLoginError = true
                    password = ""
                }
                return
            }
            onSignIn(user: user)
            email = ""
            password = ""
        }
    }
    
    public func register() {
        Auth.auth().createUser(withEmail: email, password: password) { [weak self] authResult, error in
            guard let self = self else { return }
            guard let user = authResult?.user, error == nil else {
                if let error = error {
                    self.registerErrorText = error.localizedDescription
                    showRegisterError = true
                    password = ""
                }
                return
            }
            onSignIn(user: user)
            email = ""
            password = ""
        }
    }
    
    public func signOut() {
        do {
            try Auth.auth().signOut()
            user = nil
            emailText = ""
            uid = ""
            isSignedIn = false
        } catch {
            print(error)
        }
    }
    
    public func requestVerification() {
        if let user = user {
            if !user.isEmailVerified {
                user.sendEmailVerification()
            }
        }
    }
    
    private func onSignIn(user: User) {
        emailText = "Email User: \(user.email ?? "unknow") (verified: \(user.isEmailVerified))"
        uid = "Firebase UID: \(user.uid)"
        self.user = user
        isSignedIn = true
    }
}
