//
//  AuthDataView.swift
//  Pr7
//
//  Created by Никита Пивоваров on 27.03.2024.
//

import SwiftUI

struct AuthDataView: View {
    
    @EnvironmentObject var parentViewModel: AuthViewModel
    
    var body: some View {
        VStack {
            if let user = parentViewModel.user {
                Text("Is user anonymous: \(user.isAnonymous)")
            } else {
                Text("User're not specified")
            }
        }
        .padding()
    }
}

#Preview {
    AuthDataView()
}
