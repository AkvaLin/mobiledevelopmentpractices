//
//  AuthView.swift
//  Pr7
//
//  Created by Никита Пивоваров on 27.03.2024.
//

import SwiftUI

struct AuthView: View {
    
    @ObservedObject private var viewModel = AuthViewModel()
    
    var body: some View {
        VStack(spacing: 10) {
            TextField("Login", text: $viewModel.login)
            HStack {
                if viewModel.isSecure {
                    SecureField("Password", text: $viewModel.password)
                } else {
                    TextField("Password", text: $viewModel.password)
                }
                Button("", systemImage: viewModel.isSecure ? "eye.slash" : "eye") {
                    viewModel.isSecure.toggle()
                }
            }
            .padding(.bottom)
            Button {
                viewModel.signIn()
            } label: {
                Text("Sign In")
                    .frame(maxWidth: .infinity)
            }
            .buttonStyle(.borderedProminent)
            Button("Sign Up") {
                viewModel.register()
            }
        }
        .textFieldStyle(.roundedBorder)
        .textInputAutocapitalization(.never)
        .padding()
        .fullScreenCover(isPresented: $viewModel.isSignedIn) {
            TabView {
                AuthDataView()
                    .environmentObject(viewModel)
                    .tabItem { Text("AuthData") }
                DateTimeView()
                    .tabItem { Text("WebSocket") }
                FirebaseView()
                    .tabItem { Text("Firebase") }
            }
        }
        .alert("Failed to register an account",
               isPresented: $viewModel.showRegisterError,
               actions: {
            Button("Ok") {}
        },
               message: {
            Text(viewModel.registerErrorText)
        })
        .alert("Failed to login into account",
               isPresented: $viewModel.showLoginError,
               actions: {
            Button("Ok") {}
        },
               message: {
            Text(viewModel.loginErrorText)
        })
    }
}

#Preview {
    AuthView()
}
