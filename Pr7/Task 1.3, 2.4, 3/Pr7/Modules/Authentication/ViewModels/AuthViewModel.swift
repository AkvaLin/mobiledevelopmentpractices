//
//  AuthViewModel.swift
//  Pr7
//
//  Created by Никита Пивоваров on 27.03.2024.
//

import Foundation
import FirebaseAuth

class AuthViewModel: ObservableObject {
    
    @Published var login = ""
    @Published var password = ""
    @Published var isSecure = true
    @Published var isSignedIn = false
    @Published var showLoginError = false
    @Published var showRegisterError = false
    
    private(set) var registerErrorText = ""
    private(set) var loginErrorText = ""
    private(set) var user: User?
    
    public func signIn() {
        Auth.auth().signIn(withEmail: login, password: password) { [weak self] authResult, error in
            guard let self = self else { return }
            guard let user = authResult?.user, error == nil else {
                if let error = error {
                    self.loginErrorText = error.localizedDescription
                    self.showLoginError = true
                    password = ""
                }
                return
            }
            onSignIn(user: user)
        }
    }
    
    public func register() {
        Auth.auth().createUser(withEmail: login, password: password) { [weak self] authResult, error in
            guard let self = self else { return }
            guard let user = authResult?.user, error == nil else {
                if let error = error {
                    self.registerErrorText = error.localizedDescription
                    showRegisterError = true
                    password = ""
                }
                return
            }
            onSignIn(user: user)
            login = ""
            password = ""
        }
    }
    
    private func onSignIn(user: User) {
        login = ""
        password = ""
        isSignedIn = true
        self.user = user
    }
}
