# Pr5

## Sensors

![IMAGE_DESCRIPTION](https://i.ibb.co/Vg31cSw/IMG-7127.png)

## Camera

| Camera View | System Camera | Photo taken | Photo saved |
| ------ | ------ | ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/41KgZjh/IMG-7128.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/RbqJ8Yf/IMG-7130.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/Kcn3CW6/IMG-7131.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/kxMjNwj/IMG-7132.png) |

## Compass

![IMAGE_DESCRIPTION](https://i.ibb.co/v1Lc5mL/IMG-7133.png)

## Profile with camera

| | | |
| ------ | ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/bgFW7y8/IMG-7134.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/648Nt4n/IMG-7135.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/pJYk5mT/IMG-7136.png) |

## Voice recorder

| Voice Recorder View | Recording | Recorded & Saved |
| ------ | ------ | ------ |
| ![IMAGE_DESCRIPTION](https://i.ibb.co/HXv0b5X/IMG-7137.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/VCj2BR1/IMG-7138.png) |  ![IMAGE_DESCRIPTION](https://i.ibb.co/9w80xtZ/IMG-7139.png) |
