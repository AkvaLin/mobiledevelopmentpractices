//
//  ProfileView.swift
//  Pr5
//
//  Created by Никита Пивоваров on 16.03.2024.
//

import SwiftUI

struct ProfileView: View {

    @State private var image: UIImage? = nil
    @State private var showCamera = false
    
    var body: some View {
        NavigationStack {
            List {
                Section {
                    NavigationLink {
                        VStack {
                            Text("Personal Information")
                        }
                    } label: {
                        Label(
                            title: { Text("Personal Information") },
                            icon: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 4)
                                        .fill(.gray)
                                    Image(systemName: "person.text.rectangle.fill")
                                        .resizable()
                                        .scaledToFit()
                                        .padding(4)
                                        .foregroundStyle(.white)
                                }
                                .aspectRatio(1, contentMode: .fit)
                            }
                        )
                    }
                    NavigationLink {
                        VStack {
                            Text("Sign-In & Security")
                        }
                    } label: {
                        Label(
                            title: { Text("Sign-In & Security") },
                            icon: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 4)
                                        .fill(.gray)
                                    Image(systemName: "shield.fill")
                                        .resizable()
                                        .scaledToFit()
                                        .padding(4)
                                        .foregroundStyle(.white)
                                    Image(systemName: "key.fill")
                                        .resizable()
                                        .scaledToFit()
                                        .padding(7)
                                        .foregroundStyle(.gray)
                                }
                                .aspectRatio(1, contentMode: .fit)
                            }
                        )
                    }
                    NavigationLink {
                        VStack {
                            Text("Payment & Shipping")
                        }
                    } label: {
                        Label(
                            title: { Text("Payment & Shipping") },
                            icon: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 4)
                                        .fill(.gray)
                                    Image(systemName: "creditcard.fill")
                                        .resizable()
                                        .scaledToFit()
                                        .padding(4)
                                        .foregroundStyle(.white)
                                }
                                .aspectRatio(1, contentMode: .fit)
                            }
                        )
                    }
                    NavigationLink {
                        VStack {
                            Text("Subscriptions")
                        }
                    } label: {
                        Label(
                            title: { Text("Subscriptions") },
                            icon: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 4)
                                        .fill(.gray)
                                    Image(systemName: "arrow.circlepath")
                                        .resizable()
                                        .scaledToFit()
                                        .padding(4)
                                        .foregroundStyle(.white)
                                        .rotationEffect(.degrees(90))
                                        .rotation3DEffect(
                                            .degrees(180),
                                            axis: (x: 0.0, y: 1.0, z: 0.0)
                                        )
                                    Image(systemName: "plus")
                                        .resizable()
                                        .scaledToFit()
                                        .aspectRatio(1, contentMode: .fit)
                                        .padding([.horizontal, .top], 9.5)
                                        .padding(.bottom, 8)
                                        .fontWeight(.bold)
                                        .foregroundStyle(.white)
                                }
                                .aspectRatio(1, contentMode: .fit)
                            }
                        )
                    }
                } header: {
                    HStack {
                        Spacer()
                        VStack {
                            Button {
                                showCamera.toggle()
                            } label: {
                                if let image = image {
                                    Image(uiImage: image)
                                        .resizable()
                                        .clipShape(Circle())
                                        .frame(maxWidth: 100, maxHeight: 100)
                                } else {
                                    Image(systemName: "person")
                                        .resizable()
                                        .scaledToFit()
                                        .padding()
                                        .background(.gray)
                                        .clipShape(Circle())
                                        .frame(maxWidth: 100, maxHeight: 100)
                                        .foregroundStyle(.black)
                                }
                            }
                            .padding(.top)
                            Text("Иван Иванов")
                                .font(.title)
                            Text(verbatim: "ivanivanov@ivan.com")
                                .font(.subheadline)
                                .foregroundStyle(.secondary)
                                .padding(.bottom)
                            Spacer()
                        }
                        Spacer()
                    }
                }
            }
            .headerProminence(.increased)
            .listStyle(.insetGrouped)
            .navigationTitle("Profile")
            .navigationBarTitleDisplayMode(.inline)
            .scrollBounceBehavior(.basedOnSize)
        }
        .sheet(isPresented: $showCamera, content: {
            CameraViewRepresentable(uiImage: $image)
        })
    }
}

#Preview {
    ProfileView()
}
