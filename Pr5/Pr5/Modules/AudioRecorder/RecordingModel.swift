//
//  RecordingModel.swift
//  Pr5
//
//  Created by Никита Пивоваров on 16.03.2024.
//

import Foundation

struct Recording {
    let fileURL: URL
    let createdAt: Date
}
