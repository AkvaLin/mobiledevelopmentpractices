//
//  Extensions.swift
//  Pr5
//
//  Created by Никита Пивоваров on 16.03.2024.
//

import Foundation

extension Date
{
    func toString(dateFormat format: String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

}
