//
//  Helper.swift
//  Pr5
//
//  Created by Никита Пивоваров on 16.03.2024.
//

import Foundation

func getFileDate(for file: URL) -> Date {
    if let attributes = try? FileManager.default.attributesOfItem(atPath: file.path) as [FileAttributeKey: Any],
        let creationDate = attributes[FileAttributeKey.creationDate] as? Date {
        return creationDate
    } else {
        return Date()
    }
}
