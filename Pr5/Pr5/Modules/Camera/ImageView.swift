//
//  ImageView.swift
//  Pr5
//
//  Created by Никита Пивоваров on 16.03.2024.
//

import SwiftUI

struct ImageView: View {
    
    public var imageName: String
    
    var body: some View {
        if let image = load(fileName: imageName) {
            Image(uiImage: image)
                .resizable()
                .scaledToFit()
                .clipShape(RoundedRectangle(cornerRadius: 25.0))
                .padding()
        } else {
            Text("Не удалось загрузить изображение")
        }
    }
    
    private func load(fileName: String) -> UIImage? {
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }
}
