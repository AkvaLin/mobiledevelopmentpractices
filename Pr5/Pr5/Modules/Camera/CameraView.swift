//
//  CameraView.swift
//  Pr5
//
//  Created by Никита Пивоваров on 15.03.2024.
//

import SwiftUI

var documentsUrl: URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
}

struct CameraView: View {
    @State private var isCameraPresented = false
    @State private var uiImage: UIImage?
    @State private var savedImages = [String]()
    
    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                if let uiImage = uiImage {
                    Image(uiImage: uiImage)
                        .resizable()
                        .scaledToFit()
                        .clipShape(RoundedRectangle(cornerRadius: 25.0))
                }
                HStack {
                    Button {
                        self.isCameraPresented = true
                    } label: {
                        Image(systemName: "camera")
                    }
                    if let uiImage = uiImage {
                        Spacer()
                        Button {
                            saveImageToDocumentDirectory(uiImage, fileName: "IMAGE_\(Date())")
                        } label: {
                            Image(systemName: "square.and.arrow.down")
                        }
                    }
                }
                .buttonStyle(.borderedProminent)
                Spacer()
                List {
                    ForEach(savedImages, id: \.self) { savedImage in
                        NavigationLink(savedImage) {
                            return ImageView(imageName: savedImage)
                        }
                    }.onDelete(perform: { indexSet in
                        guard let index = indexSet.first else { return }
                        do {
                            try indexSet.forEach { index in
                                try FileManager().removeItem(at: documentsUrl.appending(path: savedImages[index]))
                            }
                            savedImages.remove(atOffsets: indexSet)
                        } catch {
                            
                        }
                    })
                }
                .listStyle(.plain)
                .frame(maxHeight: 100)
            }
            .padding()
        }
        .sheet(isPresented: $isCameraPresented) {
            CameraViewRepresentable(uiImage: $uiImage)
        }
        .onAppear {
            update()
        }
    }
    
    private func update() {
        savedImages = []
        if let urls = try? FileManager().contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: [.isRegularFileKey]) {
            urls.forEach { url in
                savedImages.append(url.lastPathComponent)
            }
        }
    }
    
    private func saveImageToDocumentDirectory(_ image: UIImage, fileName: String) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
                return
            }
            
            let fileURL = documentsUrl.appendingPathComponent(fileName)
            
            do {
                try data.write(to: fileURL, options: .atomic)
                update()
                print("Изображение успешно сохранено.")
            } catch {
                print("Ошибка при сохранении изображения: \(error)")
            }
        }
    }
}

#Preview {
    CameraView()
}
