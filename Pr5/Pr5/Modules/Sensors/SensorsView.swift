//
//  SensorsView.swift
//  Pr5
//
//  Created by Никита Пивоваров on 15.03.2024.
//

import SwiftUI
import SensorKit

struct SensorsView: View {
    var body: some View {
        VStack {
            Text("Apple не предоставляет возможность запросить список всех сенсоров на устройстве. Ниже приведен список возможных сенсоров.")
                .font(.caption)
            Divider()
            List {
                Text("\(SRSensor.accelerometer.rawValue)")
                Text("\(SRSensor.ambientLightSensor.rawValue)")
                Text("\(SRSensor.ambientPressure.rawValue)")
                Text("\(SRSensor.deviceUsageReport.rawValue)")
                if #available(iOS 17.4, *) {
                    Text("\(SRSensor.electrocardiogram.rawValue)")
                }
                Text("\(SRSensor.faceMetrics.rawValue)")
                Text("\(SRSensor.heartRate.rawValue)")
                Text("\(SRSensor.keyboardMetrics.rawValue)")
                Text("\(SRSensor.mediaEvents.rawValue)")
                Text("\(SRSensor.messagesUsageReport.rawValue)")
                Text("\(SRSensor.odometer.rawValue)")
                Text("\(SRSensor.onWristState.rawValue)")
                Text("\(SRSensor.pedometerData.rawValue)")
                Text("\(SRSensor.phoneUsageReport.rawValue)")
                if #available(iOS 17.4, *) {
                    Text("\(SRSensor.photoplethysmogram.rawValue)")
                }
                Text("\(SRSensor.rotationRate.rawValue)")
                Text("\(SRSensor.siriSpeechMetrics.rawValue)")
                Text("\(SRSensor.telephonySpeechMetrics.rawValue)")
                Text("\(SRSensor.visits.rawValue)")
                Text("\(SRSensor.wristTemperature.rawValue)")
            }
            .listStyle(.plain)
        }
        .padding()
    }
}

#Preview {
    SensorsView()
}
