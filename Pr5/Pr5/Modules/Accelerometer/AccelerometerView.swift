//
//  AccelerometerView.swift
//  Pr5
//
//  Created by Никита Пивоваров on 15.03.2024.
//

import SwiftUI

struct AccelerometerView: View {
    
    @StateObject private var viewModel = AccelerometerViewModel()
    
    var body: some View {
        VStack {
            HStack {
                Text("Acc X:")
                Spacer()
                Text(viewModel.x)
                Spacer()
            }
            HStack {
                Text("Acc Y:")
                Spacer()
                Text(viewModel.y)
                Spacer()
            }
            HStack {
                Text("Acc Z:")
                Spacer()
                Text(viewModel.z)
                Spacer()
            }
        }
        .onAppear {
            viewModel.onAppear()
        }
        .onDisappear {
            viewModel.onDisappear()
        }
    }
}

#Preview {
    AccelerometerView()
}
