//
//  AccelerometerViewModel.swift
//  Pr5
//
//  Created by Никита Пивоваров on 15.03.2024.
//

import Foundation
import CoreMotion
import Combine

class AccelerometerViewModel: ObservableObject {
    
    @Published var x = ""
    @Published var y = ""
    @Published var z = ""
    
    private let motion = CMMotionManager()
    private let queue = OperationQueue()
    
    init() {
        queue.qualityOfService = .userInteractive
    }
    
    public func onAppear() {
        if motion.isDeviceMotionAvailable {
            motion.accelerometerUpdateInterval = 1.0 / 15.0
            motion.showsDeviceMovementDisplay = true
            
            motion.startAccelerometerUpdates(to: queue) { data, error in
                if let validData = data {
                    let x = validData.acceleration.x
                    let y = validData.acceleration.y
                    let z = validData.acceleration.z
                    DispatchQueue.main.async {
                        self.x = "\(x)"
                        self.y = "\(y)"
                        self.z = "\(z)"
                    }
                }
            }
        }
    }
    
    public func onDisappear() {
        if motion.isDeviceMotionAvailable {
            motion.stopAccelerometerUpdates()
        }
    }
}
