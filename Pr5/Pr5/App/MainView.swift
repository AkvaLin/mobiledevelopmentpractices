//
//  MainView.swift
//  Pr5
//
//  Created by Никита Пивоваров on 15.03.2024.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            VStack {
                SensorsView()
                AccelerometerView()
                    .padding()
            }
                .tabItem { Label(
                    title: { Text("Sensors") },
                    icon: { Image(systemName: "sensor") }
                ) }
            CameraView()
                .tabItem { Label(
                    title: { Text("Camera") },
                    icon: { Image(systemName: "camera") }
                ) }
            CompassView()
                .tabItem { Label(
                    title: { Text("Compass") },
                    icon: { Image(systemName: "safari") }
                ) }
            ProfileView()
                .tabItem { Label(
                    title: { Text("Profile") },
                    icon: { Image(systemName: "person") }
                ) }
            AudioRecorderView(audioRecorder: AudioRecorder())
                .tabItem { Label(
                    title: { Text("Recorder") },
                    icon: { Image(systemName: "mic") }
                ) }
        }
    }
}

#Preview {
    MainView()
}
