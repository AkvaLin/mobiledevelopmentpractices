//
//  Pr5App.swift
//  Pr5
//
//  Created by Никита Пивоваров on 15.03.2024.
//

import SwiftUI

@main
struct Pr5App: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
