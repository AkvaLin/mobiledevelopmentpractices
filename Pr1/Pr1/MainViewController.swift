//
//  ViewController.swift
//  MobileApplicationDevelopment
//
//  Created by Никита Пивоваров on 16.02.2024.
//

import UIKit

class MainViewController: UIViewController {
    
    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Mobile Application Development"
        lbl.font = .systemFont(ofSize: 16, weight: .ultraLight)
        lbl.textAlignment = .center
        return lbl
    }()
    
    private let loginTextField: UITextField = {
        let field = UITextField()
        field.textContentType = .password
        field.placeholder = "Login"
        field.borderStyle = .roundedRect
        return field
    }()
    
    private let passwordTextField: UITextField = {
        let field = UITextField()
        field.placeholder = "Password"
        field.borderStyle = .roundedRect
        field.isSecureTextEntry = true
        return field
    }()
    
    private lazy var lockImageButton: UIButton = {
        let bttn = UIButton(type: .contactAdd)
        let cfg = UIImage.SymbolConfiguration(weight: .semibold)
        let img = UIImage(systemName: "lock", withConfiguration: cfg)
        bttn.setImage(img, for: .normal)
        bttn.backgroundColor = .systemBlue
        bttn.tintColor = .white
        bttn.layer.cornerRadius = 10
        bttn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return bttn
    }()
    
    private lazy var signUpButton: UIButton = {
        let bttn = UIButton(type: .roundedRect)
        bttn.setTitle("Sign up", for: .normal)
        bttn.backgroundColor = .systemGray
        bttn.layer.cornerRadius = 10
        bttn.tintColor = .white
        bttn.isEnabled = false
        return bttn
    }()
    
    private let buttonsContainerView: UIStackView = {
        let view = UIStackView()
        view.distribution = .fillEqually
        view.spacing = 20
        return view
    }()
    
    private let swiftImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        let cfg = UIImage.SymbolConfiguration(textStyle: .extraLargeTitle, scale: .large)
        let image = UIImage(systemName: "swift", withConfiguration: cfg)?.withTintColor(.systemOrange)
        imgView.image = image
        imgView.tintColor = UIColor(named: "SwiftColor")
        return imgView
    }()
    
    private lazy var passwordSwitch: UISwitch = {
        let sw = UISwitch()
        sw.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        sw.addTarget(self, action: #selector(buttonTapped), for: .valueChanged)
        return sw
    }()
    
    private var constraints = [NSLayoutConstraint]()
    private var isPasswordHidden = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupViews()
        setupLandscapeConstraints()
        setupConstraints()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        orientationChanged()
    }
    
    @objc private func buttonTapped() {
        isPasswordHidden.toggle()
        titleLabel.text = isPasswordHidden ? "Secure" : "Insecure"
        passwordTextField.isSecureTextEntry = isPasswordHidden
        passwordSwitch.setOn(!isPasswordHidden, animated: true)
        let cfg = UIImage.SymbolConfiguration(weight: .semibold)
        let img = UIImage(systemName: isPasswordHidden ? "lock" : "lock.open", withConfiguration: cfg)
        lockImageButton.setImage(img, for: .normal)
    }
    
    private func setupViews() {
        view.addSubview(titleLabel)
        view.addSubview(loginTextField)
        view.addSubview(passwordTextField)
        view.addSubview(buttonsContainerView)
        view.addSubview(swiftImageView)
        buttonsContainerView.addArrangedSubview(lockImageButton)
        buttonsContainerView.addArrangedSubview(signUpButton)
        passwordTextField.addSubview(passwordSwitch)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        loginTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        buttonsContainerView.translatesAutoresizingMaskIntoConstraints = false
        swiftImageView.translatesAutoresizingMaskIntoConstraints = false
        passwordSwitch.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.deactivate(constraints)
        
        constraints = [
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            titleLabel.bottomAnchor.constraint(equalTo: loginTextField.topAnchor, constant: -20),
            
            loginTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            loginTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            loginTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -10),
            
            passwordTextField.leadingAnchor.constraint(equalTo: loginTextField.leadingAnchor),
            passwordTextField.trailingAnchor.constraint(equalTo: loginTextField.trailingAnchor),
            passwordTextField.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 20),
            
            buttonsContainerView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20),
            buttonsContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80),
            buttonsContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80),
            
            swiftImageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -20),
            swiftImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            passwordSwitch.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor, constant: -5),
            passwordSwitch.centerYAnchor.constraint(equalTo: passwordTextField.centerYAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func setupLandscapeConstraints() {
        NSLayoutConstraint.deactivate(constraints)

        constraints = [
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: -20),
            titleLabel.bottomAnchor.constraint(equalTo: passwordTextField.centerYAnchor, constant: 10),
            
            swiftImageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -20),
            swiftImageView.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor),
            
            loginTextField.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: 20),
            loginTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            loginTextField.bottomAnchor.constraint(equalTo: view.centerYAnchor, constant: -10),
            
            passwordTextField.leadingAnchor.constraint(equalTo: loginTextField.leadingAnchor),
            passwordTextField.trailingAnchor.constraint(equalTo: loginTextField.trailingAnchor),
            passwordTextField.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 20),
            
            buttonsContainerView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20),
            buttonsContainerView.leadingAnchor.constraint(equalTo: loginTextField.leadingAnchor),
            buttonsContainerView.trailingAnchor.constraint(equalTo: loginTextField.trailingAnchor),
            
            passwordSwitch.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor, constant: -5),
            passwordSwitch.centerYAnchor.constraint(equalTo: passwordTextField.centerYAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func orientationChanged() {
        let deviceOrientation = UIDevice.current.orientation
        
        switch deviceOrientation {
        case .portrait:
            setupConstraints()
            self.view.layoutIfNeeded()
        case .portraitUpsideDown:
            setupConstraints()
            self.view.layoutIfNeeded()
        case .landscapeLeft:
            setupLandscapeConstraints()
            self.view.layoutIfNeeded()
        case .landscapeRight:
            setupLandscapeConstraints()
            self.view.layoutIfNeeded()
        default:
            print("unknown orientation")
        }
    }
}

#Preview {
    MainViewController()
}

