//
//  ViewController.swift
//  MobileApplicationDevelopment
//
//  Created by Никита Пивоваров on 16.02.2024.
//

import UIKit

class ButtonViewController: UIViewController {
    
    var buttons = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        createButtons()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: view.frame.size.width / 3, height: 40)
        
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    private func createButtons() {
        for _ in 0...5 {
            let bttn = UIButton(type: .system)
            bttn.setTitle("BUTTON", for: .normal)
            bttn.backgroundColor = .systemPurple
            bttn.layer.borderWidth = 1
            bttn.layer.cornerRadius = 10
            bttn.tintColor = .white
            buttons.append(bttn)
        }
    }
}

extension ButtonViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return buttons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        myCell.contentView.addSubview(buttons[indexPath.item])
        buttons[indexPath.item].frame = CGRect(x: 0, y: 0, width: myCell.frame.width, height: myCell.frame.height)
        myCell.addSubview(buttons[indexPath.item])
        return myCell
    }
}
