//
//  TabBarController.swift
//  MobileApplicationDevelopment
//
//  Created by Никита Пивоваров on 16.02.2024.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let mainViewController = MainViewController()
        mainViewController.tabBarItem = UITabBarItem(title: "Main", image: UIImage(systemName: "swift"), tag: 0)
        
        let buttonViewController = ButtonViewController()
        buttonViewController.tabBarItem = UITabBarItem(title: "Buttons", image: UIImage(systemName: "rectangle.stack"), tag: 1)
        
        viewControllers = [
            mainViewController,
            buttonViewController
        ]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
