//
//  UserDefaultsView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

struct UserDefaultsView: View {
    
    @State private var group = UserDefaults.standard.string(forKey: "group") ?? ""
    @State private var number = UserDefaults.standard.string(forKey: "number") ?? ""
    @State private var favourite = UserDefaults.standard.string(forKey: "favourite") ?? ""
    
    var body: some View {
        VStack {
            Group {
                TextField("Номер группы", text: $group)
                TextField("Номер по списку", text: $number)
                TextField("Любимый фильм/сериал", text: $favourite)
            }
            .textFieldStyle(.roundedBorder)
            Button("Сохранить") {
                UserDefaults.standard.set(group, forKey: "group")
                UserDefaults.standard.set(number, forKey: "number")
                UserDefaults.standard.set(favourite, forKey: "favourite")
            }
            .padding(.top)
        }
        .padding()
        .toolbar {
            ToolbarItem(placement: .keyboard) {
                Button("Done") {
                    hideKeyboard()
                }
            }
        }
    }

}

#Preview {
    UserDefaultsView()
}
