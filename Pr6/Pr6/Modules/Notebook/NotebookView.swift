//
//  NotebookView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

struct NotebookView: View {
    
    @State private var name = ""
    @State private var text = ""
    
    private var documentsUrl: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    var body: some View {
        VStack(spacing: 20) {
            TextField("Название файла", text: $name)
                .padding()
                .background {
                    RoundedRectangle(cornerRadius: 18.0)
                        .stroke(.gray)
                }
            
            TextEditor(text: $text)
                .padding()
                .background {
                    RoundedRectangle(cornerRadius: 25.0)
                        .stroke(.gray)
                }                
            
            HStack(spacing: 20) {
                Button("Сохранить") {
                    guard !name.isEmpty, !text.isEmpty else { return }
                    let fileURL = documentsUrl.appendingPathComponent(name)
                    do {
                        if let data = text.data(using: .utf8) {
                            try data.write(to: fileURL, options: .atomic)
                        }
                    } catch {
                        print(error)
                    }
                }
                Button("Загрузить") {
                    guard !name.isEmpty else { return }
                    let fileURL = documentsUrl.appendingPathComponent(name)
                    do {
                        let textData = try Data(contentsOf: fileURL)
                        if let text = String(data: textData, encoding: .utf8) {
                            self.text = text
                        }
                    } catch {
                        print(error)
                    }
                }
            }
            .buttonStyle(.bordered)
            .foregroundStyle(.primary)
        }
        .padding()
        .toolbar {
            ToolbarItem(placement: .keyboard) {
                Button("Done") {
                    hideKeyboard()
                }
            }
        }
    }
}

#Preview {
    NotebookView()
}
