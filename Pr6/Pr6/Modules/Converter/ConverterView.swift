//
//  ConverterView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI
import PDFKit
import UniformTypeIdentifiers

struct ConverterView: View {
    
    @State private var text = ""
    @State private var isImporting = false
    @State private var isExporting = false
    @State private var images = [ImageData]()
    @State private var showSpinner = false
    @State private var document: PDFDocument?
    
    var body: some View {
        VStack {
            HStack {
                Button {
                    isImporting = true
                } label: {
                    Label("Choose images",
                          systemImage: "square.and.arrow.down")
                }
                Spacer()
                Button {
                    showSpinner = true
                    DispatchQueue.global(qos: .userInitiated).async {
                        document = converToPDF()
                        DispatchQueue.main.async {
                            isExporting = true
                        }
                    }
                } label: {
                    Label("Export PDF", systemImage: "square.and.arrow.up")
                }
            }
            .padding()
            if showSpinner {
                Spacer()
                ProgressView()
                Spacer()
            } else {
                ScrollView {
                    ForEach(images) { image in
                        Image(uiImage: image.image)
                            .resizable()
                            .scaledToFit()
                            .clipShape(RoundedRectangle(cornerRadius: 25.0))
                            .padding([.horizontal, .bottom])
                    }
                }
            }
        }
        .fileExporter(isPresented: $isExporting,
                      document: PDFData(pdfDocument: document ?? PDFDocument()),
                      contentTypes: [.pdf],
                      defaultFilename: "Images PDF",
                      onCompletion: { _ in
            showSpinner = false
        },
                      onCancellation: {
            showSpinner = false
        })
        .fileImporter(isPresented: $isImporting,
                      allowedContentTypes: [.image],
                      allowsMultipleSelection: true) { result in
            do {
                showSpinner = true
                let urls = try result.get()
                var images = [ImageData]()
                let group = DispatchGroup()
                var counter = 0
                
                urls.forEach { url in
                    group.enter()
                    counter += 1
                    DispatchQueue.global(qos: .userInteractive).async {
                        let result = read(from: url)
                        let data = try? result.get()
                        if let data = data, let image = UIImage(data: data) {
                            images.append(ImageData(image: image))
                            group.leave()
                        }
                    }
                }
                
                group.notify(queue: .main) {
                    self.images = images
                    showSpinner = false
                }
            } catch {
                print(error)
            }
        } onCancellation: {
            showSpinner = false
        }
    }
    
    private func read(from url: URL) -> Result<Data,Error> {
        let accessing = url.startAccessingSecurityScopedResource()
        defer {
            if accessing {
                url.stopAccessingSecurityScopedResource()
            }
        }
        
        return Result { try Data(contentsOf: url) }
    }
    
    private func converToPDF() -> PDFDocument {
        let document = PDFDocument()
        images.enumerated().forEach { (index, image) in
            if let page = PDFPage(image: image.image) {
                document.insert(page, at: index)
            }
        }
        return document
    }
}

struct ImageData: Identifiable {
    let id = UUID()
    let image: UIImage
}

struct PDFData: FileDocument {
    
    var pdfDocument: PDFDocument = PDFDocument()
    static var readableContentTypes: [UTType] = [.pdf]
    
    init(pdfDocument: PDFDocument) {
        self.pdfDocument = pdfDocument
    }
    
    init(configuration: ReadConfiguration) throws {
        if let data = configuration.file.regularFileContents {
            pdfDocument = PDFDocument(data: data) ?? PDFDocument()
        }
    }
    
    func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper {
        let data = pdfDocument.dataRepresentation() ?? Data()
        return FileWrapper(regularFileWithContents: data)
    }
}

#Preview {
    ConverterView()
}
