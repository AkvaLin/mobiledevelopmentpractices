//
//  EmployeeView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI
import SwiftData

struct EmployeeView: View {
    
    @Query private var models: [EmployeeModel]
    @Environment(\.modelContext) private var modelContext
    
    @State private var name = ""
    @State private var salary = ""
    
    var body: some View {
        
        VStack(spacing: 10) {
            TextField("Enter name", text: $name)
            TextField("Enter salary", text: $salary)                
            Button("Save") {
                guard
                    !name.isEmpty,
                    !salary.isEmpty,
                    let salary = Int(salary)
                else { return }
                
                let model = EmployeeModel(name: name, salary: salary)
                modelContext.insert(model)
                name = ""
                self.salary = ""
            }
            .buttonStyle(.borderedProminent)
            List(models) { model in
                VStack(alignment: .leading) {
                    Text(model.name)
                    HStack {
                        Group {
                            Text("Salary:")
                            Text("\(model.salary)")
                        }
                        .font(.footnote)
                        Spacer()
                    }
                }
                .swipeActions {
                    Button("Delete", role: .destructive) {
                        modelContext.delete(model)
                    }
                    Button("Update") {
                        guard
                            !name.isEmpty,
                            !salary.isEmpty,
                            let salary = Int(salary)
                        else { return }
                        
                        model.name = name
                        model.salary = salary
                        
                        name = ""
                        self.salary = ""
                    }
                    .tint(.blue)
                }
            }
            .listStyle(.inset)
            .padding(.top)
        }
        .padding()
        .textFieldStyle(.roundedBorder)
        .toolbar {
            ToolbarItem(placement: .keyboard) {
                Button("Done") {
                    hideKeyboard()
                }
            }
        }
    }
}

#Preview {
    EmployeeView()
}
