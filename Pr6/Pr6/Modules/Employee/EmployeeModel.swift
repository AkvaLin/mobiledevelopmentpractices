//
//  EmployeeModel.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import Foundation
import SwiftData

@Model
final class EmployeeModel {
    let id: UUID
    var name: String
    var salary: Int
    
    init(name: String, salary: Int) {
        self.id = UUID()
        self.name = name
        self.salary = salary
    }
}
