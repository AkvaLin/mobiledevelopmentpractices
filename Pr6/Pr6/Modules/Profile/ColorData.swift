//
//  ColorData.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

struct ColorData {
    private let userDefaults = UserDefaults.standard
    
    func saveColor(color: Color, key: String) {
        let color = UIColor(color).cgColor
        
        if let components = color.components {
            userDefaults.set(components, forKey: key)
        }
    }
    
    func loadColor(for key: String) -> Color? {
        guard let array = userDefaults.object(forKey: key) as? [CGFloat] else { return nil }
        
        let color = Color(
            .sRGB,
            red: array[0],
            green: array[1],
            blue: array[2],
            opacity: array[3]
        )
        
        return color
    }
}
