//
//  ProfileView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

struct ProfileView: View {
    
    @State private var backgroundColor: Color
    @State private var foregroundColor: Color
    @State private var name: String
    @State private var email: String
    
    private let colorData: ColorData
    
    init() {
        self.colorData = ColorData()
        self.backgroundColor = colorData.loadColor(for: "profileImageBackgroundColor") ?? .gray
        self.foregroundColor = colorData.loadColor(for: "profileImageForegroundColor") ?? .black
        self.name = UserDefaults.standard.string(forKey: "profileName") ?? ""
        self.email = UserDefaults.standard.string(forKey: "profileEmail") ?? ""
    }
    
    var body: some View {
        Form {
            Section {
            } header: {
                HStack {
                    Spacer()
                    VStack {
                        Image(systemName: "person")
                            .resizable()
                            .scaledToFit()
                            .padding()
                            .background(backgroundColor)
                            .foregroundStyle(foregroundColor)
                            .clipShape(Circle())
                            .frame(maxWidth: 100, maxHeight: 100)
                            .foregroundStyle(.black)
                        Text(name)
                            .font(.title)
                        Text(verbatim: email)
                            .font(.subheadline)
                            .foregroundStyle(.secondary)
                            .padding(.bottom)
                    }
                    Spacer()
                }
                .headerProminence(.increased)
            }
            Section("Profile data") {
                HStack {
                    TextField("Name", text: $name)
                    Button("Save") {
                        UserDefaults.standard.set(name, forKey: "profileName")
                    }
                }
                HStack {
                    TextField("Email", text: $email)
                    Button("Save") {
                        UserDefaults.standard.set(email, forKey: "profileEmail")
                    }
                }
            }
            .alignmentGuide(.listRowSeparatorLeading) { _ in 0 }
            Section("Profile image") {
                ColorPicker("Background", selection: $backgroundColor)
                ColorPicker("Foreground", selection: $foregroundColor)
                Button("Save") {
                    colorData.saveColor(color: backgroundColor, key: "profileImageBackgroundColor")
                    colorData.saveColor(color: foregroundColor, key: "profileImageForegroundColor")
                }
            }
        }
        .formStyle(.grouped)
        .scrollBounceBehavior(.basedOnSize)
        .toolbar {
            ToolbarItem(placement: .keyboard) {
                Button("Done") {
                    hideKeyboard()
                }
            }
        }
    }
}

#Preview {
    ProfileView()
}
