//
//  KeychainView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

struct KeychainView: View {
    
    @State private var name: String? = {
        do {
            guard let result = try KeychainManager.getName() else { return nil }
            guard let stringName = String(data: result, encoding: .utf8) else { return nil }
            return stringName
        } catch {
            print(error)
            return nil
        }
    }()
    
    init() {
        guard let name = "Александр Александрович Блок".data(using: .utf8) else { return }
        do {
            try KeychainManager.save(name: name)
        } catch {
            print(error)
        }
    }
    
    var body: some View {
        VStack {
            Image(.blok)
                .resizable()
                .scaledToFit()
                .clipShape(RoundedRectangle(cornerRadius: 25.0))
                .shadow(radius: 10)
            Text(name ?? "")
                .font(.title3)
                .fontWeight(.light)
                .padding(.top)
        }
        .padding()
    }
}

enum KeychainError: Error {
    case duplicateItem
    case unknow(status: OSStatus)
}

final class KeychainManager {
    static func save(name: Data) throws {
        let query: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: "poet",
            kSecValueData: name
        ]
        
        let status = SecItemAdd(query as CFDictionary, nil)
        
        guard status != errSecDuplicateItem else { 
            throw KeychainError.duplicateItem
        }
        
        guard status == errSecSuccess else {
            throw KeychainError.unknow(status: status)
        }
    }
    
    static func getName() throws -> Data? {
        let query: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: "poet",
            kSecReturnData: kCFBooleanTrue as Any
        ]
        
        var result: AnyObject?
        
        let status = SecItemCopyMatching(query as CFDictionary, &result)
        
        guard status == errSecSuccess else {
            throw KeychainError.unknow(status: status)
        }
        
        return result as? Data
    }
}

#Preview {
    KeychainView()
}
