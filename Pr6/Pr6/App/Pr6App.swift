//
//  Pr6App.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI

@main
struct Pr6App: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
