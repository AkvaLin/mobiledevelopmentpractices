//
//  MainView.swift
//  Pr6
//
//  Created by Никита Пивоваров on 18.03.2024.
//

import SwiftUI
import SwiftData

struct MainView: View {
    var body: some View {
        TabView {
            VStack {
                UserDefaultsView()
                KeychainView()
            }
            .tabItem { Text("Defaults/Keychain") }
            NotebookView()
                .tabItem { Text("Notebook") }
            EmployeeView()
                .tabItem { Text("Employee") }
                .modelContainer(for: EmployeeModel.self)
            ProfileView()
                .tabItem { Label(
                    title: { Text("Profile") },
                    icon: { Image(systemName: "person") }
                ) }
            ConverterView()
                .tabItem { Label(
                    title: { Text("Convertor") },
                    icon: { Image(systemName: "photo") }
                ) }
        }
    }
}

#Preview {
    MainView()
}
